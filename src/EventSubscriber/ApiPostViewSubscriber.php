<?php
namespace App\EventSubscriber;

use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\Book;
use App\Entity\Post;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Class ApiPostViewSubscriber
 * @package App\EventSubscriber
 */
final class ApiPostViewSubscriber implements EventSubscriberInterface
{
    private $em;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => ['countViews', EventPriorities::PRE_SERIALIZE],
        ];
    }

    /**
     * Adds view to Post made from the API
     * @param GetResponseForControllerResultEvent $event
     */
    public function countViews(GetResponseForControllerResultEvent $event)
    {
        $request = $event->getRequest();
        $route = $request->get('_route');
        if ($route == 'api_posts_get_item') {
            $post = $event->getControllerResult();
            if ($post instanceof Post) {
                $post->addView();
                $this->em->persist($post);
                $this->em->flush();
            }
        }
    }
}
