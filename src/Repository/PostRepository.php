<?php

namespace App\Repository;

use App\Entity\Post;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * Class PostRepository
 * @package App\Repository
 */
class PostRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Post::class);
    }

    public function getAllActiveQuery()
    {
        return $this->createQueryBuilder('p')
            ->leftJoin('p.tags', 'tags')
            ->andWhere('p.isActive = :status')
            ->setParameter('status', true)
            ->orderBy('p.publishedAt', 'DESC')
            ->getQuery();
    }
}
