<?php

namespace App\Controller\App;

use App\Repository\PostRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Knp\Component\Pager\PaginatorInterface;

/**
 * Class IndexController
 * @Route("/")
 * @package App\Controller\App
 */
class IndexController extends AbstractController
{
    /**
     * @Route("/", methods={"GET"}, name="app_index")
     */
    public function index(PostRepository $postRepo,Request $request, PaginatorInterface $paginator)
    {
        $postsQb = $postRepo->getAllActiveQuery();
        $pagination = $paginator->paginate(
            $postsQb,
            $request->query->getInt('page', 1),
            2
        );

        return $this->render('app/index/index.html.twig', ['postsPagination' => $pagination]);
    }


}
