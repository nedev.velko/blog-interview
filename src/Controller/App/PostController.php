<?php

namespace App\Controller\App;

use App\Entity\Post;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;


/**
 * Class PostController
 * @Route("/post")
 * @package App\Controller\App
 */
class PostController extends AbstractController
{
    /**
     * @Route("/{slug}", methods={"GET"}, name="app_post_view")
     */
    public function view(Post $post)
    {
        //TODO refactor this to a service that takes into account the IP of the visitor
        //TODO take as an identifier of visitor not only IP (because of multiple devices using one IP)
        $post->addView();
        $em = $this->getDoctrine()->getManager();
        $em->persist($post);
        $em->flush();

        return $this->render('app/post/view.html.twig', ['post' => $post]);
    }


}
