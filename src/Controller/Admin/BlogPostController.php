<?php

namespace App\Controller\Admin;

use App\Entity\Post;
use App\Form\PostType;
use App\Repository\PostRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class BlogPostController
 * @Route("/admin/blog-post")
 * @package App\Controller\Admin
 */
class BlogPostController extends AbstractController
{
    const NEW_BLOG_POST_ROUTE = 'admin_blog_post_new';
    const LIST_BLOG_POST_ROUTE = 'admin_blog_post_list';

    /**
     * @Route("/list", methods={"GET"}, name="admin_blog_post_list")
     * @param PostRepository $postRepo
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function list(PostRepository $postRepo)
    {
        $posts = $postRepo->findBy([], ['id' => 'ASC']);

        return $this->render('admin/blogPost/list.html.twig', ['posts' => $posts]);
    }

    /**
     * @Route("/new", methods={"GET", "POST"}, name="admin_blog_post_new")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function new(Request $request)
    {
        $post = new Post();
        $form = $this->createForm(PostType::class, $post);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($post);
            $em->flush();
            $this->addFlash('success', 'The blog post is created successfully!');

            return $this->redirectToRoute(self::LIST_BLOG_POST_ROUTE);
        }

        return $this->render('admin/blogPost/new.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/{id}/edit", methods={"GET", "POST"}, name="admin_blog_post_edit")
     * @param Post $post
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function edit(Post $post, Request $request)
    {
        $form = $this->createForm(PostType::class, $post);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($post);
            $em->flush();
            $this->addFlash('success', 'The blog post is edited successfully!');

            return $this->redirectToRoute(self::LIST_BLOG_POST_ROUTE);
        }

        return $this->render('admin/blogPost/new.html.twig', ['form' => $form->createView(), 'post' => $post]);
    }

    /**
     * @Route("/{id}/delete", methods={"GET"}, name="admin_blog_post_delete")
     * @param Post $post
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function delete(Post $post, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        try {
            $em->remove($post);
            $em->flush();
            $this->addFlash('success', 'The blog post is deleted successfully!!');
        } catch (\Exception $exception) {
            $this->addFlash('danger', 'The blog post is cannot be deleted. Reason: ' . $exception->getMessage());
        }

        return $this->redirectToRoute('admin_blog_post_list');
    }
}
