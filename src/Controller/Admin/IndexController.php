<?php

namespace App\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class IndexController
 * @Route("/admin")
 * @package App\Controller\Admin
 */
class IndexController extends AbstractController
{
    /**
     * @Route("/", methods={"GET"}, name="admin_index")
     */
    public function index()
    {
        return $this->render('admin/index/index.html.twig');
    }
}
