<?php

namespace App\Serializer;

use ApiPlatform\Core\Serializer\SerializerContextBuilderInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ApiContextBuilder
 * This service hooks into the process of context building for the api platform
 * and adds serialization group depending on if it's item or collection operation
 *
 * @package App\Serializer
 */
final class ApiContextBuilder implements SerializerContextBuilderInterface
{
    private $decorated;

    public function __construct(SerializerContextBuilderInterface $decorated)
    {
        $this->decorated = $decorated;
    }

    public function createFromRequest(Request $request, bool $normalization, ?array $extractedAttributes = null): array
    {
        $context = $this->decorated->createFromRequest($request, $normalization, $extractedAttributes);
        if (isset($extractedAttributes['item_operation_name'])) {
            $context['groups'][] = 'full_read';
        }

        return $context;
    }
}
