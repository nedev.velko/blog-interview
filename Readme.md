Symfony Blog interview application (by Velko)
========================

This is a project developed with Symfony 4.2.
It's a blog and consists of 3 main parts:
- admin panel
- public pages
- public API

Requirements
------------

  * PHP 7.1.3 or higher;
  * MySql 5.7
  * and the [usual Symfony application requirements][2].

Installation
------------

Checking out from the repository

```bash
$ git clone https://gitlab.com/nedev.velko/blog-interview.git
```

 Then execute the following command from the checkout dir 
 to install vendor library packets

```bash
$ composer install
```

 In the MySQL server must be created database `db_blog` with
 utf8_general_ci encoding and configure the .env file in the project
 to connect to it
 
 ```
 DATABASE_URL=mysql://db_user:db_password@127.0.0.1:3306/db_blog
 ```
 
 Because of the project being interview task for simplicity can be used
 the build in PHP server which can be started from the project dir using:
 ```
 php bin/console server:run
 ```
 
 Then the database structure must be created. This is done with the Doctrine
 Migrations.
 ```
 php bin/console doctrine:migrations:migrate
 ```
 
 Then example data can be loaded which includes 'admin user, example blog and tags'.
 Fixtures are located in /fixtures
 ```
 php bin/console hautelook:fixtures:load
 ```
 
 - The project is accessible in the browser on 127.0.0.1:8000/
 
 - The user from the fixtures is 'bloguser' with password 'admin42'
 
 - The API has documentation and can be tested (make requests) on the 
 following address: 127.0.0.1:8000/api/docs
 



